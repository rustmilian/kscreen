# Translation of kcm_kscreen.po to Catalan
# Copyright (C) 2014-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2015, 2018, 2019, 2020, 2021, 2022, 2023.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2016, 2017, 2018, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-04 00:38+0000\n"
"PO-Revision-Date: 2023-12-28 07:51+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#: output_model.cpp:96
#, kde-format
msgid "%1 Hz"
msgstr "%1 Hz"

#: output_model.cpp:640
#, kde-format
msgctxt "Width x height"
msgid "%1x%2"
msgstr "%1x%2"

#: output_model.cpp:659
#, kde-format
msgctxt "Width x height (aspect ratio)"
msgid "%1x%2 (%3:%4)"
msgstr "%1x%2 (%3:%4)"

#: output_model.cpp:740
#, kde-format
msgid "None"
msgstr "Cap"

#: output_model.cpp:747
#, kde-format
msgid "Replicated by other output"
msgstr "Replicat per una altra sortida"

#: ui/main.qml:41
#, kde-format
msgid "Keep display configuration?"
msgstr "Mantinc la configuració de la pantalla?"

#: ui/main.qml:56
#, kde-format
msgid "Will revert to previous configuration in %1 second."
msgid_plural "Will revert to previous configuration in %1 seconds."
msgstr[0] "Es revertirà a la configuració anterior en %1 segon."
msgstr[1] "Es revertirà a la configuració anterior en %1 segons."

#: ui/main.qml:73
#, kde-format
msgid "&Keep"
msgstr "&Mantén"

#: ui/main.qml:86
#, kde-format
msgid "&Revert"
msgstr "&Reverteix"

#: ui/main.qml:106 ui/main.qml:287
#, kde-format
msgctxt "@info"
msgid "All displays are disabled. Enable at least one."
msgstr "Totes les pantalles estan desactivades. Almenys activeu-ne una."

#: ui/main.qml:108 ui/main.qml:289
#, kde-format
msgctxt "@info"
msgid ""
"Gaps between displays are not supported. Make sure all displays are touching."
msgstr ""
"No es permeten separacions entre les pantalles. Assegureu-vos que totes les "
"pantalles estan en contacte."

#: ui/main.qml:120 ui/main.qml:302
#, kde-format
msgid "A new output has been added. Settings have been reloaded."
msgstr "S'ha afegit una sortida nova. S'ha recarregat la configuració."

#: ui/main.qml:122 ui/main.qml:304
#, kde-format
msgid "An output has been removed. Settings have been reloaded."
msgstr "S'ha eliminat una sortida. S'ha recarregat la configuració."

#: ui/main.qml:172
#, kde-format
msgid "No KScreen backend found. Please check your KScreen installation."
msgstr "No s'ha trobat el dorsal del KScreen. Comproveu la instal·lació."

#: ui/main.qml:182
#, kde-format
msgid "Outputs could not be saved due to error."
msgstr "No s'han pogut desar les sortides per un error."

#: ui/main.qml:192
#, kde-format
msgid ""
"Global scale changes will come into effect after the system is restarted."
msgstr ""
"Els canvis de l'escala global tindran efecte després de reiniciar el sistema."

#: ui/main.qml:198
#, kde-format
msgid "Restart"
msgstr "Reinicia"

#: ui/main.qml:218
#, kde-format
msgid "Display configuration reverted."
msgstr "S'ha revertit la configuració de la pantalla."

#: ui/main.qml:226
#, kde-format
msgctxt "@title:window"
msgid "Change Priorities"
msgstr "Canvi de prioritats"

#: ui/main.qml:255 ui/OutputPanel.qml:44
#, kde-format
msgid "Primary"
msgstr "Primària"

#: ui/main.qml:260
#, kde-format
msgid "Raise priority"
msgstr "Eleva la prioritat"

#: ui/main.qml:270
#, kde-format
msgid "Lower priority"
msgstr "Redueix la prioritat"

#: ui/Orientation.qml:14
#, kde-format
msgid "Orientation:"
msgstr "Orientació:"

#: ui/Orientation.qml:28 ui/OutputPanel.qml:155 ui/OutputPanel.qml:195
#, kde-format
msgid "Automatic"
msgstr "Automàtica"

#: ui/Orientation.qml:43
#, kde-format
msgid "Only when in tablet mode"
msgstr "Només en el mode tauleta"

#: ui/Orientation.qml:59
#, kde-format
msgid "Manual"
msgstr "Manual"

#: ui/Output.qml:238
#, kde-format
msgid "Replicas"
msgstr "Rèpliques"

#: ui/OutputPanel.qml:26
#, kde-format
msgid "Enabled"
msgstr "Habilitada"

#: ui/OutputPanel.qml:37
#, kde-format
msgid "Change Screen Priorities…"
msgstr "Canvia les prioritats de les pantalles…"

#: ui/OutputPanel.qml:50
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This determines which screen your main desktop appears on, along with any "
"Plasma Panels in it. Some older games also use this setting to decide which "
"screen to appear on.<nl/><nl/>It has no effect on what screen notifications "
"or other windows appear on."
msgstr ""
"Això determina en quina pantalla apareixerà l'escriptori principal, junt amb "
"els plafons del Plasma. Alguns jocs més antics també utilitzen aquesta opció "
"per a decidir en quina pantalla apareixeran.<nl/><nl/>No té cap efecte a on "
"apareixeran les notificacions de pantalla o altres finestres."

#: ui/OutputPanel.qml:55
#, kde-format
msgid "Resolution:"
msgstr "Resolució:"

#: ui/OutputPanel.qml:75
#, kde-kuit-format
msgctxt "@info"
msgid "&quot;%1&quot; is the only resolution supported by this display."
msgstr "L'única resolució admesa per aquesta pantalla és «%1»."

#: ui/OutputPanel.qml:85
#, kde-format
msgid "Scale:"
msgstr "Escala:"

#: ui/OutputPanel.qml:114 ui/Panel.qml:105
#, kde-format
msgctxt "Global scale factor expressed in percentage form"
msgid "%1%"
msgstr "%1%"

#: ui/OutputPanel.qml:126
#, kde-format
msgid "Refresh rate:"
msgstr "Freqüència d'actualització:"

#: ui/OutputPanel.qml:146
#, kde-format
msgid "\"%1\" is the only refresh rate supported by this display."
msgstr ""
"L'única freqüència d'actualització admesa per aquesta pantalla és «%1»."

#: ui/OutputPanel.qml:151
#, kde-format
msgid "Adaptive sync:"
msgstr "Sincronització adaptativa:"

#: ui/OutputPanel.qml:154
#, kde-format
msgid "Never"
msgstr "Mai"

#: ui/OutputPanel.qml:156
#, kde-format
msgid "Always"
msgstr "Sempre"

#: ui/OutputPanel.qml:167
#, kde-format
msgid "Overscan:"
msgstr "Sobreexploració:"

#: ui/OutputPanel.qml:176
#, kde-format
msgctxt "Overscan expressed in percentage form"
msgid "%1%"
msgstr "%1%"

#: ui/OutputPanel.qml:189
#, kde-format
msgid "RGB range:"
msgstr "Interval RGB:"

#: ui/OutputPanel.qml:196
#, kde-format
msgid "Full"
msgstr "Complet"

#: ui/OutputPanel.qml:197
#, kde-format
msgid "Limited"
msgstr "Limitat"

#: ui/OutputPanel.qml:213
#, kde-format
msgctxt "@label:textbox"
msgid "Color Profile:"
msgstr "Perfil de color:"

#: ui/OutputPanel.qml:221
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter ICC profile path…"
msgstr "Introduïu el camí del perfil ICC…"

#: ui/OutputPanel.qml:237
#, kde-format
msgctxt "@action:button"
msgid "Select ICC profile…"
msgstr "Seleccioneu el perfil ICC…"

#: ui/OutputPanel.qml:248
#, kde-format
msgid "Opens a file picker for the ICC profile"
msgstr "Obre un selector de fitxer per al perfil ICC"

#: ui/OutputPanel.qml:257
#, kde-format
msgctxt "@title:window"
msgid "Select ICC Profile"
msgstr "Selecció de perfil ICC"

#: ui/OutputPanel.qml:282
#, kde-format
msgctxt "@info:tooltip"
msgid "ICC profiles aren't compatible with HDR yet"
msgstr "Els perfils ICC encara no són compatibles amb HDR"

#: ui/OutputPanel.qml:287
#, kde-format
msgctxt "@label"
msgid "High Dynamic Range:"
msgstr "Marge dinàmic elevat:"

#: ui/OutputPanel.qml:292
#, kde-format
msgctxt "@option:check"
msgid "Enable HDR"
msgstr "Activa l'HDR"

#: ui/OutputPanel.qml:298
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"HDR allows compatible applications to show brighter and more vivid colors. "
"Note that this feature is still experimental"
msgstr ""
"HDR permet que les aplicacions compatibles mostrin colors més brillants i "
"vius. Tingueu en compte que aquesta característica encara és experimental"

#: ui/OutputPanel.qml:309
#, kde-format
msgctxt "@label"
msgid "SDR Brightness:"
msgstr "Lluminositat de l'SDR:"

#: ui/OutputPanel.qml:328
#, kde-format
msgctxt "@info:tooltip"
msgid "Sets the brightness of non-HDR content on the screen, in nits"
msgstr "Estableix la lluminositat del contingut no HDR a la pantalla, en nits"

#: ui/OutputPanel.qml:339
#, kde-format
msgctxt "@label"
msgid "SDR Color Intensity:"
msgstr "Intensitat de color de l'SDR:"

#: ui/OutputPanel.qml:365
#, kde-format
msgctxt "Color intensity factor expressed in percentage form"
msgid "%1%"
msgstr "%1%"

#: ui/OutputPanel.qml:373
#, kde-format
msgctxt "@info:tooltip"
msgid "Increases the intensity of non-HDR content on the screen"
msgstr "Augmenta la intensitat del contingut no HDR a la pantalla"

#: ui/OutputPanel.qml:378
#, kde-format
msgid "Replica of:"
msgstr "Rèplica de:"

#: ui/Panel.qml:27
#, kde-format
msgid "Device:"
msgstr "Dispositiu:"

#: ui/Panel.qml:69
#, kde-format
msgid "Global scale:"
msgstr "Escala global:"

#: ui/Panel.qml:129 ui/Panel.qml:149
#, kde-format
msgid "Legacy applications (X11):"
msgstr "Aplicacions antigues (X11):"

#: ui/Panel.qml:134
#, kde-format
msgctxt "The apps themselves should scale to fit the displays"
msgid "Apply scaling themselves"
msgstr "Aplica el seu propi canvi d'escala"

#: ui/Panel.qml:139
#, kde-format
msgid ""
"Legacy applications that support scaling will use it and look crisp, however "
"those that don't will not be scaled at all."
msgstr ""
"Les aplicacions antigues que permeten el canvi d'escala l'utilitzaran i es "
"veuran nítides, però les que no ho facin no s'escalaran en absolut."

#: ui/Panel.qml:150
#, kde-format
msgctxt "The system will perform the x11 apps scaling"
msgid "Scaled by the system"
msgstr "Canvi d'escala pel sistema"

#: ui/Panel.qml:155
#, kde-format
msgid ""
"All legacy applications will be scaled by the system to the correct size, "
"however they will always look slightly blurry."
msgstr ""
"El sistema farà el canvi d'escala de totes les aplicacions antigues a la "
"mida correcta, tanmateix, sempre semblaran lleugerament borroses."

#: ui/Panel.qml:160
#, kde-format
msgctxt "@label"
msgid "Screen tearing:"
msgstr "Esquinçament de pantalla:"

#: ui/Panel.qml:163
#, kde-format
msgctxt ""
"@option:check The thing being allowed in fullscreen windows is screen tearing"
msgid "Allow in fullscreen windows"
msgstr "Permet en finestres a pantalla completa"

#: ui/Panel.qml:168
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Screen tearing reduces latency with most displays. Note that not all "
"graphics drivers support this setting."
msgstr ""
"L'esquinçament de pantalla redueix la latència amb la majoria de pantalles. "
"Tingueu en compte que no tots els controladors gràfics admeten aquesta opció."

#: ui/Panel.qml:182
#, kde-format
msgid ""
"The global scale factor is limited to multiples of 6.25% to minimize visual "
"glitches in applications using the X11 windowing system."
msgstr ""
"El factor d'escala global està limitat a múltiples de 6,25% per a minimitzar "
"els errors visuals a les aplicacions usant el sistema de finestres X11."

#: ui/RotationButton.qml:51
#, kde-format
msgid "90° Clockwise"
msgstr "90° en sentit horari"

#: ui/RotationButton.qml:55
#, kde-format
msgid "Upside Down"
msgstr "Cara avall"

#: ui/RotationButton.qml:59
#, kde-format
msgid "90° Counterclockwise"
msgstr "90° en sentit antihorari"

#: ui/RotationButton.qml:64
#, kde-format
msgid "No Rotation"
msgstr "Sense gir"

#: ui/ScreenView.qml:48
#, kde-format
msgid "Drag screens to re-arrange them"
msgstr "Arrossega les pantalles per a reordenar-les"

#: ui/ScreenView.qml:61
#, kde-format
msgid "Identify"
msgstr "Identificació"
